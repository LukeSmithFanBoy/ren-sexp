#+title: Мысли насчёт релизов

* 0.1.0 Самый базовый UX/UI
Сегодняшняя неделя была сплошь такой "гештальт терапией".  Сделано очень много старых хотелок, вид движка очень сильно приблизился к тому каким я хотел его видеть.  Перед тем как изливать остальные мысли, наглядно покажу функционал:

1. Теперь есть бегающая перед текстом каретка:

[[https://files.catbox.moe/rt0ch0.webm][клик меня]]
2. А так же можно писать несколько строчек на одном экране

[[https://files.catbox.moe/fw9zfu.webm][клик меня]]

3. Для пользователей -- всё.  Остальное подкапотные реализации.

** История движка
Это надо куда-то написать, пускай будет в начале.

Для начала я должен сказать, что текущая архитектура движка завелась у меня с третьего раза.  Первая итерация была с использованием DOM и ООП подхода, когда каждая сущность была объектом, которому надо было послать сообщение.  В начале казалось это чем-то хорошим, но быстро оказалось, что это путь в никуда.  Слишком много пришлось контролировать последовательности вызовов, всякие переменные и ожидания завершения функций.

Потом я решил, что вместо того что бы контролировать стейт, лучше держать его в одном месте, и каждый кадр заменять прежний узел на новособранный, для этого есть функционал [[https://developer.mozilla.org/en-US/docs/Web/API/Element/replaceWith][replaceWith]].  Но от ООП и ДОМика я так и не отказался тогда.

В итоге пришел к тому, что дом обновлять очень долго, и что связываться с ним гиблое дело.  И вернулся (впервые) к шаблону от Дэвида Томпсона, где используется подход через canvas, а не DOM.  После исследований оказалось что это явлется стандартом в гейм деве под веб.

Сначала просто вывел текст масенький.  Потом фон показал, потом спрайты...  Проблема была только в том, что делал я всё это в последние дни гейм джема.  В итоге вместо очень продуманного сюжета пришлось придумать сюжет _буквально_ за 2 часа.

** Общие впечатления от сентябрьской разработки
Первые пол месяца я долго настривался что бы быстро поехать.  Первый коммит, тот который позволяет запускать игру на Hoot 0.5.0, был создан 15 сентября.

Ощущения менялись с утомления при добавлении буквально любой фичи в потоковую работу в последние дни, когда работающий код получался за 2-3 запуска, и переиспользовалось максимальное количество кода.

Работа напрямую с циклом обработки экрана + функциональный стиль позволяют мне не городить костыли вокруг абстрактных объектов (ну пожалуйста фреймворк ну нарисуй мне тут как надо, ну прошу, ты ведь можешь это? напиши что от меня нужно я всё сделаю только скажи что-нибудь...), а просто делать то что мне нужно.

** RSSL
Хочу подчеркнуть одну из самых важных вещей для улучшения пользовательского (с точки зрения разработки сюжета) опыта -- разработка RSSL.

Он же RenSexp Scripting Language -- типо ДСЛ для написания сценариев не через схемные структурки, а через что-то человеко-читаемое.

Задача самая неоднозначная.  Сначала было непонято как подойти.  Поначалу набрасывал в файликах примеры, на доске фломасетрами.  Как часто бывает, через пару подходов начал ощущать какой именно API хочу -- мне необходимо было иметь некую изначальную сцену, которую бы я потом как-то изменял в процессе.  Но пробрасывать её между функциями было бы очень утомительно.

Изначально хотел реализовать всю эту тему через [[https://okmij.org/ftp/Scheme/macros.html#ck-macros][CK-macros]], но сейчас нет времени в нем разбираться.  Зато после общения с одним из подписчиков в комментариях, накидал один пример, после которого сразу понял что мне нужно -- трединг макрос.

Завести на бегу [[https://srfi.schemers.org/srfi-197/srfi-197.html][srfi-197]] под хут у меня не получилось, зато получилось завести макрос с другого [[https://daviddavidson.website/threading-macros-scheme/][сайта]].  В итоге получилось что-то такое:
#+begin_src scheme
  (define script
    (->> (list WELCOME)
	 (CLEAN)
	 (BG      bg:kitchen)
	 (BGM     music:curious_critters)
	 (PAUSE   100)
	 (JOIN    (list masha:happy nastya:tired))
	 (TXT     "And here the water is ready.")

	 (UPDATE  nastya:tired nastya:normal)
	 (TXT     "My patience has ended!")

	 (UPDATE  masha:happy masha:troubled)
	 (TXT     "Wait a little bit longer, please.")

	 (UPDATE  nastya:normal nastya:tired)
	 (TXT     "I don't want to wait, pour it faster.")
	 ...))
#+end_src

Стиль трединга из кложи мне нравится больше, так как не надо явно прописывать позицию аргумента, как это сделано в srfi.

Больше всего мне нравится что этот подход не заставляет все переписывать с нуля, он просто красиво обвалакивает уже существующий функционал, _вообще_ ничего не добавляя в сам движок.  Тут есть даже ключевое слово JUST, которое позволяет подсунуть в список сцен уже существующую структуру сцены, вообще никак её не модифицируя.  Этот JUST я использовал что бы на новый лад не переписывать сцены WELCOME и END.

Кстати про капс в этом DSL.  Да, тут всё на капсе, что бы максимально мимикрировать под (негласные) [[https://fountain.io/syntax/][стандарты]] написания сценария.  Ну и ещё это всё олдскульно выглядит.  Вроде как раньше вообще не было разграничений на капс и прописные буквы, все всё писали именно что капсом.

Так же мне нравится что не пришлось обмазываться define-syntax и его друзьями.  Макросы конечно не выглядят сверх-сложно, но они у меня вызывают подозрения.  Может в конце концов придётся использовать этих друзей.

Например, когда понадобится расширять язык дальше.  По моей задумке DSL должен покрывать каждый аспект написания сценария так, что бы автору скрипта не понадобится изучать язык программирования (но с ручным "поднятием солнца" на небосводе, по желанию).  Сомневаюсь что это можно решить только лишь трединг макросом.

Другое направление развития -- использование [[https://www.draketo.de/software/wisp][Wisp]].  Тогда писать скрипт будет максимально приятно, и максимально безпроблемно для новичков, которые могут запутаться в скобках.

Самое приятное -- что это можно получить абсолютно бесплатно, ведь wisp это просто библиотека.  Scheme Power!

** Новое название
Мне немного не нравится название движка.  RenSexp звучит не так благозвучно как RenPy, и с этим надо что-то делать.  С одной стороны движок должен подчеркивать особенности внутренней реализации, куда входит Scheme, с другой что-то говорить о культурной преемственности.

Сейчас крутится идея как-то использовать в названии NScripter но... обычно часть имени берут от него именно форки (ONScripter, ONScripter-En).  ..Может написать интерпритатор языка для написания сценариев NS?  Пока непонятно, но название поменять хочется.

Может.... ZScripter? :)  И расширение сразу напрашивается .zs, которое [[https://en.wikipedia.org/wiki/List_of_filename_extensions_(S%E2%80%93Z)][используется]] только для каких-то там модов для модов в минекрафт.

** Предстоящая работа
Добавить хочется много что, но если оформлять это всё в сроки и релизы, то к следующему хочется видеть в движке больше видов переходов между сценами и какой-то HUD, где человек может начать новую игру, или продолжить существующую.  Ну и докинуть прождение по истории назад тоже можно добавить.  Наверно в первую очередь.

Скорее всего, для этого приёдтся как-то разделять сцены -- на /постоянные/ можно будет ходить по истории, а через эфемерные, игрок будет переступать.  Вообще это всё слишком размывает понятие сцена, давно хочу переименовать эту сущность во Frame, или чето такое.

*** Эффекты
Хочется добавить всякие интересные переходы, по типу "поднятия занавеса", "свайпа влево" и "свайпа вправо", встряски экрана.

*** Внутриигровое меню
Считаю что пока стоит ограничиться именно внешним меню, то есть не тем которое отркывается по нажатию ESCAPE, а именно при открывании игры которое показыается.  Там будут кнопочки "начать", "продолжить", "О игре" и "выйти".

*** Сохранение
Как таковое сохранение реализовать я не понимаю как.  Можно посмотреть как это сделано в других играх, но сомневаюсь что там используются какие-то прогрессивые подходы.

*** Рефакторинг
Так же хочется закрыть баги и [[https://gitlab.com/LukeSmithFanBoy/ren-sexp/-/issues/26][почистить код от констант]].  + Попробовать отойти от подхода с состоянием игры как глобальной переменной.  Если я в будущем захочу использовать асинхронные подходы для реализации каких-то фич, то эта глобальная переменная может здоворо мне помешать двигаться дальше.  Пока у меня в планах хранить состояние игры в каком-то канале, который смогут читать все интересующиеся.

А само это новое состояние изменять только в функции update, той самой которая вызывается столько же раз сколько фпс в игре.  Сейчас есть небольшая проблемка с тем, что новое состояние формируется помимо функции update, так же через нажатие пробела.  Хотя может это выдуманная проблема.

** Что дальше
Вплоть до окончания гейм джема новые фичи добавляться не будут.  Твердо и Четко.  Остались задачи плана чисто внешнего вида -- подправить ассеты, раставить лицензии в заголовки и атрибушены правильно упаковать.

Что касается жизни, сейчас надо заняться более серьёзно математикой -- линейной алгеброй.

Может быть, попробую запилить демку, в которой некий персонаж Инструктор будет показывать игроку возможности движка.

Может быть, скину код на ревью Дэвиду, он ведь сказал что может помогать всем участникам джема.

Возможно, в процессе разработки демки с демонстрацией функционала, я найду критические проблемы, что поможет мне заниматься исключительно игрой во время предстоящего game jam.
